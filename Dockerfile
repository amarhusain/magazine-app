FROM python:3.11-slim
WORKDIR /app
ADD . /app
RUN pip install --trusted-host pypi.python.org Flask
ENV NAME Amar
CMD ["python", "app.py"]